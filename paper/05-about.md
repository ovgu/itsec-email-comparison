Diese Arbeit wurde im Rahmen des wissenschaftlichen Seminars IT-Sicherheit an der Otto-von-Guericke-Universität Magdeburg im Juni 2019, unter der Leitung von Prof. Jana Dittmann und Robert Altschaffel angefertigt.

Die Autoren sind die folgenden Bachelorstudenten der Informatikfakultät:

&nbsp;

> Moritz Marquardt  
> `moritz.marquardt@st.ovgu.de`

> Jonas Hielscher  
> `benedikt.hielscher@st.ovgu.de`

> Niklas Baier  
> `niklas.baier@st.ovgu.de`

> Rawan Jouli  
> `rawan.jouli@st.ovgu.de`

---

\begin{center}
Otto-von-Guericke-Universität Magdeburg\\
Universitätsplatz 2\\
39106 Magdeburg
\end{center}

\newpage
