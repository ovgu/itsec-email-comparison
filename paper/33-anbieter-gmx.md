\newpage

## GMX

GMX hat in Deutschland einen Marktanteil von 24,5%[^ProviderStatistik] (18 Millionen Nutzer) und ist damit Marktführer unter den E-Mail Providern. Zusammen mit den Schwesteranbieter Web.de (15 Millionen Nutzer) und 1&1 Mail kommen die 1&1 E-Mail Provider auf einen Marktanteil von über 50%. Datenschutz- und Sicherheitsprobleme auf diesem Portal haben deshalb Auswirkungen auf eine große Anzahl Nutzer. Es fällt dabei auf, dass GMX erst im Juni 2019 das 2-Faktor-Authentifizierungs-Feature eingeführt hat[^HeiseGmxSecurity] und damit Jahre hinter der Konkurrenz wie Hotmail, GMail, Yahoo! zurückliegt. GMX bietet das Versenden von sicheren DE-Mails an[^deMail]. 

### Kritik

GMX steht bei Datenschützern vor allem wegen der offensiven Werbung und der häufigen Vertragsunterbreitung für Website Besucher in der Kritik[^focusGmxKritik]. Die 2-Faktor-Authentifizierung wurde 2019 erst eingeführt, nachdem zahlreiche Nutzerkonten von Doxern übernommen wurden. Gegen die GMX Serverinfrastruktur gab es in der Vergangenheit mehrfach erfolgreiche Angriffe[^gmxHackzwei].

### Funktionen

GMX bietet als Unternehmen der 1&1 Unternehmensgruppe eine Vielzahl von Diensten, die nicht direkt mit einem E-Mail-Postfach zusammenhängen (z.B. einen eigenen Browser, Handyverträge, Cloud Speicher, eigene Domains, Homepagebaukasten). 

Für E-Mail-Postfächer stehen die drei Vertragsoptionen Free, Pro und Top zur Verfügung. In der freien Version stehen 1,5 GB E-Mail Speicherplatz und 20 MB Anhänge pro E-Mail zur Verfügung. Auch 2GB Cloud Speicherplatz werden geboten. Pro und Top erweitern diese Begrenzungen jeweils und fügen einige weitere Features hinzu. Auch das Abschalten von Werbebannern und Popups soll in den bezahlten Versionen enthalten sein.

![GMX Vergleich der Tarife](images/GmxVergleichDerTarife.PNG)

### Sicherheit

Sowohl auf der Startseite als auch nach einem Login gibt es keinerlei unverschlüsselten Verbindungen zu dem GMX Server, oder zu Drittanbietern. Alle HTTP Aufrufe, aber auch alle nachgeladenen Ressourcen (JavaScript, CSS Dateien, Bilder, Videos, Schriften) werden verschlüsselt übertragen. Eine unverschlüsselte Version der Seite unter http://gmx.de steht nicht zur Verfügung und es erfolgt eine Weiterleitung auf die verschlüsselte Seite. Laut dem SSL Labs Report[^sslLabGmx] erhält gmx.de die positive Note A+. 

GMX bietet seit Juni 2016 Zweifaktorauthentifizierung mittels Authentifizierungsapp  an. GMX speichert E-Mails nach eigenen Angaben ausschließlich in deutschen Rechenzentren[^gmxRechenzentren]. Ob die E-Mail Inhalte dort verschlüsselt gespeichert werden ist nicht bekannt. Das Ende-zu-Ende verschlüsselte Senden von E-Mails mit PGP wird unterstützt. Dafür ist Installieren des Drittanbieter Browser Plugins Mailvelope notwendig[^gmxMailVerschluesselung].

Nutzer, die sich 12 Monate lang nicht in ihre Accounts einloggen verlieren ihre E-Mail-Adresse. Dies ist bedenklich, wenn die Adresse neu vergeben wird und weiterhin Nachrichten an den ursprünglichen Nutzer gesendet werden[^gmxDoxing].

### Erstellen eines Nutzerkontos

Ein GMX Account berechtigt zum Nutzen des Freemail Services, aber auch für das Buchen von Premium Paketen und Domains. Um ein Account anzulegen ist es zwingend erforderlich seine Adresse und sein Geburtsdatum anzugeben. Auch eine zweite E-Mail-Adresse, oder eine Mobilfunknummer muss zur Passwortwiederherstellung angegeben werden. Das Angeben von Sicherheitsfragen zum Zurücksetzen des Passworts, ist nicht mehr erforderlich. Auch Bank- oder Ausweisdaten werden in der freien Version nicht gefordert.

*Autor: Jonas Hielscher*

[^ProviderStatistik]: https://de.statista.com/statistik/daten/studie/151754/umfrage/nutzeranteile-von-e-mail-anbietern-in-deutschland/ abgerufen am 28.06.2019
[^HeiseGmxSecurity]: https://www.heise.de/newsticker/meldung/GMX-fuehrt-sichere-Anmeldung-mit-Zwei-Faktor-Authentifizierung-ein-4439558.html abgerufen am 28.06.2019
[^deMail]: https://de-mail.info/ abgerufen am 28.06.2019
[^sslLabGmx]: https://www.ssllabs.com/ssltest/analyze.html?d=gmx.de&latest abgerufen am 28.06.2019
[^gmxSecurityInformationen]: https://www.gmx.net/mail/sicherheit/ abgerufen am 28.06.2019
[^gmxRechenzentren]: https://www.gmx.net/mail/sicherheit/pgp/#.pc_page.produktseiten.mail.nav_5.sicherheit_pgp abgerufen am 28.06.2019
[^gmxMailVerschluesselung]: https://img.ui-portal.de/cms/gmx/produkte/sicherheit/pgp/lp/Anleitung-Verschluesselte-Kommunikation-GMX.pdf abgerufen am 28.06.2019
[^focusGmxKritik]: https://www.focus.de/digital/internet/nutzer-beschweren-sich-verbraucherschuetzer-warnen-vor-gmx-und-web-de_id_7940035.html abgerufen am 28.06.2019
[^gmxDoxing]: https://www.nw.de/blogs/games_und_netzwelt/22347107_0rbit-knackte-viele-E-Mail-Konten-Wie-unsicher-sind-GMX-Web.de-und-Co..html abgerufen am 28.06.2019
[^gmxHackzwei]: https://www.heise.de/newsticker/meldung/GMX-Hackerangriff-durch-fatales-Schlupfloch-31708.html abgerufen am 28.06.2019

> Jonas Hielscher  
> `benedikt.hielscher@st.ovgu.de`
