## Google Mail

### Versuchsprotokoll

 Die Untersuchung von Google Mail wurde unter Windows 10 (Version 1809 - Enterprise) im IP Adressbereich der Otto-von-Guericke-Universität Magdeburg durchgeführt. Für die Mehrzahl der Untersuchungen wurde ein neu installierter Chromium Browser verwendet (Version 77.0.3824.0).

### Ergebnisse

 Google Mail zeigt  keinerlei Werbungen ohne Zulassung des Nutzers.

#### Drittanbieter

App-Entwickler, die Teil des Gmail-Programms von Google sind, dürfen diese E-Mails unter bestimmten Umständen lesen, um neue Dienste oder App-Funktionen zu erstellen, heißt es. Der Großteil der E-Mails würde automatisch per Computersoftware gescannt, in einigen Fällen sollen aber menschliche Mitarbeiter mitgelesen haben. Return Path, ein Unternehmen, das sich E-Mail-Marketing auf die Fahnen geschrieben hat, hat angeblich die Posteingänge von mehr als 2 Millionen Menschen gescannt und 8000 E-Mails ausspionieren lassen.


#### Versandte Daten

 Durch das Anfertigen und Absenden von E-Mails werden keine Daten aus dem Browser gesendet. Der Inhalt von der geschriebenen E-Mail wird lediglich per Post Request an einen G-Mail Server versendet.
 

#### Verschlüsselung

 Mit S/MIME wird eine E-Mail verschlüßelt und während der Zustellung unterstützt. Die ausgehenden E-Mails werden automatisch verschlüsselt, wann immer dies möglich ist.

#### Datenschutz, Transparenz

- Die personenbezogenen Daten einschließlich der Gmail- und Google-Kontoinformationen werden von Google nicht verkauft. Sie geben keinerlei personenbezogene Daten an Werbetreibende weiter, es sei denn, der Nutzer wünscht dies.

- Sie achten außerdem sehr sorgfältig auf die Art der Inhalte, für die sie Werbung schalten. Google wird beispielsweise keine Werbung auf der Grundlage sensibler Informationen wie Hautfarbe, Religion, sexueller Orientierung, Gesundheit oder sensibler Finanzdaten schalten. Die in Gmail geschaltete Werbung unterliegt den Gmail-Werberichtlinien.

- Wenn der Nutzer festlegen möchte, dass keine auf Ihren personenbezogenen Daten basierende Werbung für ihn in Gmail angezeigt wird, rufen er die Seite "Einstellungen für Werbung" auf und deaktivieren Sie dann die Option "Weitere Werbung deaktivieren". Wenn der Nutzer diese Funktion deaktiviert, kann es dennoch vorkommen, dass Gmail-Werbung eingeblendet wird. Diese bezieht sich jedoch nicht mehr auf personenbezogene Daten, die mit Ihrem Google-Konto verknüpft sind.
  
![](images/GmailDatenschutz.png)


> Rawan Jouli  
> `rawan.jouli@st.ovgu.de`
