
\newpage

# Beurteilung & Zusammenfassung

| Kriterium                       | Google Mail | Outlook   | GMX       | T-Online |
| ------------------------------- | ----------- | --------- | --------- | -------- |
| Drittanbieter                   | 1           | min. 7    | min. 57   | min. 70  |
| Sessions                        | N/A         | ja        | nein      | nein     |
| Schweregrad                     | 5           | 5         | 2         | 2        |
| Unterschiede in Firefox         | N/A         | keine     | keine     | keine    |
| Effektivität der Browserhärtung | N/A         | teilweise | teilweise | nein     |
| Vollständigkeit                 | ja          | nein      | nein      | nein     |
| Transparenz                     | 4           | 4         | 3         | 3        |

Durch die Analyse in dieser Arbeit ist eine Gegenüberstellung der untersuchten Anbieter in Hinblick auf Drittanbieter in Web Clients möglich. Es ist dabei klar erkennbar, dass keiner der untersuchten Anbieter nach unseren Kriterien vollständig vertrauenswürdig ist. Während GMX durch das Nichtanlegen von Sessions positiv hervorsticht ist die Menge an eingebunden Drittanbieterverbindungen, genau wie bei T-Online, negativ zu beurteilen. Google und Outlook laden zwar kaum Ressourcen von anderen Domains nach, verarbeiten und verkaufen die Daten (bzw. im Fall von Google Mail nur daraus erworbene Erkentnisse) jedoch direkt. Nach unserer Metrik ist GMX der privatsphärefreundlichste der von uns untersuchten Anbieter, wobei wir dennoch zur Verwendung eines Anbieters wie mailbox.org, protonmail.com oder tutanota.com raten, die garantiert keinen einzigen Drittanbieter im Postfach einsetzen.

> Moritz Marquardt & Jonas Hielscher  
> `moritz.marquardt@st.ovgu.de` & `benedikt.hielscher@st.ovgu.de`
