\newpage

## T-Online Mail

### Versuchsaufbau

Die Untersuchung des T-online Web Mail Clients wurde unter Ubuntu 18.10 im Universitätsnetzwerk der Otto-von-Guericke-Universität Magdeburg durchgeführt. Dazu wurde ein neu installierter Chromium Browser (Version 75.0.3770.90) verwendet, der von https://download-chromium.appspot.com/ als "Chromium for Linux x64" offiziell heruntergeladen wurde. An den Voreinstellungen des Browsers wurden keine Veränderungen vorgenommen. Als einziges zusätzlich installiertes Addon kam uMatrix für Chrome[^uMatrixChromium] (Version 1.3.16) zum Einsatz. Dieses Addon wurde so eingestellt, dass es Verbindungen visualisiert, jedoch nicht blockiert.

Einige Testergebnisse wurden mit zwei anderen Set-ups abgeglichen.

1. Der selbe Chromium Browser mit voll aktiviertem uMatrix Plugin.
2. Firefox Browser (Version 67.0.4 64bit), unter einem neuen Profil gestartet und damit ohne zusätzliche Addons.


### Ergebnisse
Die Ergebnisse beruhen auf Stichproben, da durch das Laden von meistens anderer Werbung, auch andere Anbieter eingebunden sind. Es fehlt Determinismus und damit wiederholbare Situationen. Die Anzahl von Requests bzw. Größe von übertragenen Daten ist schwer genau zu sagen, da teilweise periodisch live nachgeladen wird. Aussagen über Anzahlen spiegeln, wenn nicht anders beschrieben, den Durchschnitt aus den Untersuchungen wieder.
Bis auf das Kapitel "Drittanbieter", welches in 3 Bereiche der Webseite unterteilt ist, beziehen sich alle Ergebnisse auf das Postfach.

### Struktur
Im Verlaufe der Untersuchung ist oft aufgefallen, dass der Aufbau und die Verlinkung zwischen den Webseiten durcheinander bzw. sogar kaputt erschien. Es wurden veraltete sowie defekte Links gefunden und es gab mehrere verschiedene Wege sich einzuloggen, teilweise mit Login-Fenstern in unterschiedlichem Design (neu und älter). Außerdem ist auch unterschiedlich viel Tracking eingebunden.

![Login Methode 1, über "E-Mail-Konto" auf Startseite](images/LoginMethode1.png.png)

![Login Methode 2, über "Freemail sichern" auf Startseite, "anmelden"](images/LoginMethode2.png)

![Login Methode 3, über "Kundenzentrum" "Login"](images/LoginMethode3.png)

Dabei fiel auf, dass alle Login Seiten über eine lange URL mit vielen anscheinend dynamisch generierten Parametern erreicht werden. 
Alle laufen über eine Version von https://accounts.login.idm.telekom.com, dies direkt aufzurufen liefert allerdings einen 403 Error, keine Berechtigung. Somit können die Login-Seiten leider nicht über Online-Tools getestet werden.

### Drittanbieter
#### Startseite t-online.de
Obwohl die T-online Startseite t-online.de gekauft wurde und, im Gegensatz zu den Telekomdiensten wie T-online Mail, von Ströer betrieben wird, wurde diese für einen Ersteindruck analysiert. Es war nicht klar, wie stark Ströer die Technik der Seite umgebaut hatte, neben der eigentlichen Versorgung mit Artikeln. Dieser Ersteindruck sollte sich tatsächlich später im Maildienst fortsetzen.

T-online.de, welche als Nachrichtendienst mit eigenen Artikeln fungieren soll, hat eine enorme Anzahl an Verbindungsaufbauten.

![viele Verbindungen in uMatrix erkennbar, zu erkennen am Scroll-balken](images/StartseiteuMatrix.png).

In den Untersuchungen waren es durchschnittlich ca. 70 Verbindungen. Laut Chromiums Button für Informationen über die Verbindung wurden ca. 140-170 Cookies nach initialem Laden der Webseite benutzt.

Das T-online Portal zeigt auf jeder Seite große Mengen an Drittanbieter-Werbung an. Ein Aufruf von t-online.de lädt im Schnitt 4,5 MB unterschiedlicher Medien herunter.

Eine Untersuchung mittels PrivacyScore brachte 126 eingebundene Drittanbieter hervor, wovon 105 bekannte Tracker sind. Außerdem wurden 24 kurzfristige und 120 langfristige Cookies gefunden, wobei 115 davon von 40 bekannten Tracker gesetzt wurden. Bekannte Sicherheitslücken wurden nicht gefunden.

#### Login
Bei Untersuchung der Login-Seite, welche man über das Kundenzentrum findet, wurde gleich bei Aufruf JavaScript entdeckt, welches "Phoenix Login Tracking" als Halter des Copyrights erwähnt.
Nachdem die eigene E-Mail-Adresse eingegeben und bestätigt wurde, wurden neue JavaScript-Skripte geladen. Hierbei fiel direkt auf, dass die Namen alle auf bekannte große Customer Analytics-Plattformen hindeuten.
- utag.js für Tealium, mit ihrem "Tealium universal tag" (Zählpixel, für Werbung/Profiling, „collect unique visitor behavior information“)
- u.a. adfaction für Adform
- u.a. Inhalte von Webtrekk
Alle JavaScript-Skripte wurden auf Root-Ebene ausgeführt und hatten somit theoretisch Zugriff auf den gesamten Inhalt der Seite.

![geladenes Javascript bei Login](images/LoginJS.png)

uMatrix zeigte Werbefirmen, Taggingfirmen und CDN's an.

![uMatrix im Login-Bereich](images/LoginuMatrix.png){height=300}

Im letzten Schritt, nach Eingabe des Passworts sowie Bestätigung, wurden neben den vorherigen Trackern auch noch "Crypto-JS" geladen, somit wurde die eigene IP an code.google.com/p/crypto-js übermittelt, wodurch Google Kenntnis vom Login bekommen hatte.

Schon hier, bei Login, fällt bei genauerer Betrachtung der JavaScript-Dateien auf, dass fast alle minimiert und somit beinahe unmöglich nachzuvollziehen sind. Dies kann aus Gründen der Optimierung von Speichernutzung und Übertragungsgeschwindigkeit oder für Verschleierung durchgeführt werden, wobei mit modernen Techniken diese Art von Variablennamenersetzung durch einzelne Buchstaben, nur noch minimalen Nutzen hat und nur noch für Giganten wie Google mit einer riesigen Anzahl von Abrufen einen Unterschied macht.

#### Postfach
Im Postfach und damit auch während des Lesens und Schreibens von E-Mails, sind sehr viele Drittanbieter Ressourcen in die Webseite eingebunden. Darunter viele bekannte Tracker. Dabei handelt es sich um Große, wie Microsoft durch Bing, Google durch doubleclick/Google Analytics und Walmart sowie viele Kleinere. Außerdem ändern sie sich nach jedem Neuladen der Seite.

Insgesamt erkannte uMatrix immer eine extrem lange Liste. Da uMatrix jedoch keine Export-Funktion anbietet und sich das Fenster für die Ergebnisse nicht vergrößern lässt, war die Dokumentierung dieser Liste schwierig. Als die Liste jedoch einmal gezählt wurde, ließen sich 114 unterschiedliche Drittanbieter finden.

uMatrix zeigte außerdem initial 106 Third-Party-Cookies und 85 Third-Party-XHRs an. Da XHR als REST Calls die Möglichkeit bieten, darüber die größten Datenmengen und verzögert zu laden, werden sie besonders beachtet. Unter den XHRs befanden sich hauptsächlich folgende 4 Anbieter:
- as-sec.casalemedia.com mit 18
- securepubads.g.doubleclick.net mit 21
- yieldlove-d.openx.net mit 18
- fastlane.rubiconproject.com mit 18

Laut Chromium wurden initial 100 Cookies benutzt, welche dann in Schritten von 20 bis 60 wuchsen. Die übertragene Datenmenge betrug, inklusive der Inhalte, die direkt von t-online.de kamen, initial 5,3 MB Download und 110 KB Upload. Sie wuchs dann mit jedem heruntergeladenen Werbebanner und Skript.

Das Besondere am Postfach ist der Fakt, dass zyklisch alle 30'000ms neue Werbung nachgeladen und Updates durchgeführt werden. Ein immer wiederkehrendes JavaScript war "gpt.js". Dies steht für Google Publisher-Tags und ist eine Bibliothek für Tag-Kennzeichnung für das Management von Werbung sowie Tracking durch Tracking-Pixel.

![zyklisches Ausführen von Skripten und Holen von Werbung](images/PostfachZyklisch.png)

Durch die periodische Aktualisierung wächst die Anzahl benutzter Cookies sowie die übertragene Datenmenge.


**Nun** folgt noch eine Erwähnung einiger bemerkenswerter Hintergrundvorgänge und Einbindungen, die bei den Untersuchungen aufgefallen waren:

JavaScript war auch im Postfach oft minimiert und somit beinahe unmöglich nachvollziehbar. Unter anderem https://www.googletagservices.com/activeview/js/current/osd_listener.js?cache=r20110914.

Häufig wurden Trackingpixeln als 1x1 gif geholt. Dabei wurden unterschiedliche eindeutige IDs übertragen.

![Tracking Pixel als 1x1 Data](images/PostfachTrackingPixel.png)

Es wurde oft auf Serverantwort gewartet. Wahrscheinlich für Server-Updates auf meetrics.net.

![auf Serverantwort wartend](images/PostfachPending.png)

Meetrics.net war auch der Hauptlieferant für Tracking-Pixel, mit über 100 geladenen Grafiken.

Es wurde die Einbindung des JavaScript-Programmes "Confiant" festgestellt. Es lief auf Root Ebene und hatte damit vollen Zugriff auf die Inhalte der Postfach-Seite. Bei Recherche stellte sich heraus, dass es sich um eine Art Virenscanner gegen infizierte Werbung handelt[^confiant]. Es wurde von einem CDN geladen:

```
https://clarium.global.ssl.fastly.net/?wrapper=2DPge-WVhZFIVEVo0laRH9eF5JI&tpid=MkRQZ2UtV1ZoWkZJVkVWbzBsYVJIOWVGNUpJL3B1Ym1hdGljOjMwMHg2MDA%3D&d=eyJ3aCI6Ik1rUlFaMlV0VjFab1drWkpWa1ZXYnpCc1lWSklPV1ZHTlVwSkwzQjFZb TFoZEdsak9qTXdNSGcyTURBPSIsIndkIjp7ImsiOnsiaGJfYmlkZGVyIjpbInB1Ym   1hdGljIl0sImhiX3NpemUiOlsiMzAweDYwMCJdfX0sIndyIjowfQ==
```

Das Script von ioam.de sowie das von Webtrekk lief auf Root-Ebene. Webtrekk-Einbindung wurde durch Nutzung von responder.wt-safetag.com erkannt.

### Versandte Daten
Durch die regelmäßige Aktualisierung der Werbung sowie das Horchen auf Serverupdates, entstand sehr viel und häufiges Hintergrund-Rauschen, was die Überprüfung erschwert hatte. Allerdings konnte keine Aufzeichnung oder Sendung von Echtzeitdaten nachgewiesen werden. Es werden zwar viele bekannte Tracker eingebunden, doch schneiden diese das Nutzerverhalten nicht live mit. Dafür werden jedoch Werbebanner mit eindeutigen IDs geladen.

So wurde beispielsweise hier eine eindeutige UID übertragen:

```
https://image2.pubmatic.com/AdServer/Pug?piggybackCookie=uid:0f3f3d87-0fc9-4468-bc47-df237b59a3f2&vcode=bz0yJnR5cGU9MSZjb2RlPTMwNjImdGw9MjAxNjA=
```

### Mail Versand

Es konnte nicht festgestellt werden, dass Teile einer E-Mail an Drittanbieter gehen.

### Sessions
Es konnte zu keinem Zeitpunkt festgestellt werden, dass Drittanbieter LocalStorage oder SessionStorage mit identifizierenden Einträgen benutzen. T-online setzt jedoch selber zahlreiche Cookies, die zur eindeutigen Identifizierung eines Nutzers geeignet sind.

### Öffnen von E-Mails
Es gibt keine Funktion um Bilder bei Öffnen einer Mail erst einmal zurückzuhalten oder ähnliches. Somit werden beim Öffnen sofort alle Ressourcen geladen und Tracking, durch unter Anderem Tracking-Pixel, ist möglich.

### Schweregrad
Es wurden zumindest eindeutige ID's geladen und verschickt.

Die JavaScript-Skripte, welche auf Root-Ebene liefen und damit vollen Zugriff auf alle Inhalte hatten, waren zumindest direkt im Quellcode der Seite zu finden, wodurch eine Note abgezogen wird. 

**T-online erhält hierfür die Schulnote 2.**

### Verschlüsselung
Alle Inhalte werden über verschlüsselte Verbindungen übertragen, genau so wie es im Kapitel über die Vorstellung des Anbieters schon festgestellt wurde.

### Effektivität der Browserhärtung
Bei aktiviertem uMatrix, der einen Großteil der Drittanbieter blockt, funktionieren trotzdem alle Funktionen der Webseite bezüglich des Maildienstes ohne Probleme. Es gibt keine Warnmeldungen. Werbung ist effektiv blockiert. Allerdings sind CSS und vor Allem Grafiken von Drittanbietern, welche nicht auf der internen Liste bekannter schädlicher Webseiten sind, noch erlaubt. Somit wurden die über 100 Grafiken von meetrics.net noch geladen, wodurch Tracking-Pixel in den Browser kamen. Die übertragene Datenmenge reduzierte sich damit auf 2,9 MB Download und 290 B Upload, was eine merkliche Einsparung war und sich durch weniger ausgeführtes JavaScript auch an der CPU-Auslastung bemerkbar machte. Somit ist Browserhärtung sehr empfehlenswert.

### Unterschiede in Firefox
Es konnte kein signifikanter Unterschied zwischen einem nicht modifizierten Firefox und einem Chromium Browser festgestellt werden. Diese Untersuchung wird aber auch durch fehlenden Determinismus der Drittanbietereinbindungen erschwert. Die bekanntesten Tracker (z.B. doublecklick.net) wurden in beiden Browsern nicht geblockt.

### Vollständigkeit
T-online Mail wirbt offensiv mit deutschen Datenschutzstandards, vor allem aufgrund der Initiative "E-Mail made in Germany". Hinweise auf die Verwendung von Cookies fehlen hingegen, sowohl auf der Startseite als auch im Postfach, obwohl Cookies benutzt werden. Somit gibt es auch keine weiteren Einstellungen für die Cookies.

### Transparenz
Die Telekom bietet viele Stellen für ihre unterteilte Datenschutzerklärung an, um die entsprechende Erklärung für spezifische Fälle zu erhalten[^erkl] sowie eine vereinfachte Version für den schnellen Überblick[^dschutz]. Der "Datenschutzhinweis für Telekom Mail" in deutsch, unter https://www.telekom.de/datenschutzhinweise/download/041.pdf zu erreichen, beinhaltet 2373 Wörter.

Es wird die Benutzung von Tracking-Tools und Einbindung von Werbetreibenden aufgeführt und mit dem Finanzieren kostenlos angebotener Dienste erklärt.
Es wird immer betont, dass wenn Werbetreibende ein Profil anlegen, keine Identifizierung der einzelnen Person möglich sein soll.
Der wichtige Abschnitt 8. "Wird mein Online-Nutzungsverhalten durch Werbe-Drittanbieter ausgewertet?" ist jedoch zu knapp gehalten und schiebt die Verantwortung den Werbetreibenden zu, obwohl die Telekom bewusst diese Anbieter für ihre Zwecke einbindet und sich damit Gedanken über die Situation machen sollte, derer sie ihre Kunden aussetzt: "Dabei setzen Werbe-Anbieter auch Maßnahmen zur Erfassung von Online-Nutzungsdaten ein. Die Erfassung dieser Daten liegt nicht in der Verantwortung der Telekom Deutschland GmbH." Dafür wird auf den Einsatz von Werbe- und Trackingblocker immerhin hingewiesen und uBlock Origin erwähnt, was begrüßenswert ist.
Es wird keine Verarbeitung von E-Mail-Inhalten erwähnt, wobei nicht ersichtlich ist, ob dies auf die zu knappen Ausführungen zurückzuführen ist.

Die Telekom und damit T-online Mail zeigt personalisierte Werbung an. Es ist nicht möglich dies abzuschalten. Es kann zwar die komplette Werbung abgeschalten werden, jedoch nur über den kostenpflichtigen Vertrag Mail M oder als Vertragsmail als Kunde.

Es ist eine vereinfachte Version verfügbar und der Allgemeine sowie der Datenschutzhinweis speziell für Telekom Mail sind recht kurz und verständlich gehalten. Fachwörter werden erklärt und das komplizierteste, was nicht erklärt wird, sind Begriffe wie "Tracking" und "pseudonymisiert" welche zu erweitertem Grundwissen gehören sollten. Jedoch wird auf manch wichtige Aspekte nicht genau genug eingegangen.
**T-online Mail erhält hierfür die Schulnote 3.**

### Fazit
In der Freemail-Variante trackt T-online Nutzer und lässt tracken, um personalisierte Werbung anzeigen zu können sowie das Freemail-Angebot zu finanzieren. Dies wird in den Datenschutzbestimmungen eindeutig aufgezeigt, kann jedoch nicht abgeschalten werden bzw. nur durch Kauf.
Zumindest bei der Datenübertragung wird Acht gegeben auf Unterstützung aktueller Sicherheitsanforderungen und alle Daten werden verschlüsselt übertragen. Nutzerverhalten wird nicht live getrackt, z.B. in Form von Tastenanschlägen. Jedoch können Drittanbieter  potenziell erfahren, wenn Nutzer sich einloggen und E-Mails lesen oder versenden. Der Inhalt der E-Mails bleibt dabei aber geheim.
Die immer noch fehlende 2-Faktor-Authentifizierung andererseits ist peinlich und die Struktur der Dienste, wozu auch T-online Mail und sein Login gehören, ist durcheinander. Die enorme Anzahl an Einbindungen von Drittanbieter auf allen Seiten lässt am Durchblick der Seitenbetreiber an ihrer eigenen Webseite zweifeln. Es sollte fast unmöglich sein, genau zu wissen, wer alles was sammelt. Dadurch entsteht auch ein Sicherheitsrisiko, falls eine der großen oder vor Allem der kleinen Firmen gehackt oder kompromittiert wird und damit Zugriff auf Teile des Fensters des Nutzers entsteht. Dies verstärkt sich durch die Einbindung von undurchsichtigem JavaScript und vielen XHRs.

Somit ist der Nutzer einem Risiko ausgesetzt und es ist schlecht für Privatsphäre, Geräteauslastung und Netzwerkverkehr.
T-online Freemail ist daher nicht empfehlenswert.


> Niklas Baier  
> `niklas.baier@st.ovgu.de`

[^confiant]: https://www.confiant.com/ abgerufen am 04.07.2019
[^erkl]: https://www.telekom.de/datenschutzhinweise
[^dschutz]: https://www.telekom.de/start/datenschutz