# Abstract

Wir untersuchen die in Deutschland weit verbreitenden E-Mail-Anbieter GMX, T-Online, Outlook und Google Mail auf ihre Datenschutzkonformität. Dazu untersuchen wir den Datenverkehr der jeweiligen Web-Clients, die Einbindung von Drittanbietern und die Datenschutzbestimmungen. 

Mithilfe eines selbst erarbeiteten Bewertungsschemas vergleichen wir so das Verhalten der Online-Postfächer dieser Anbieter, sowie den Inhalt derer Datenschutzbestimmungen.
