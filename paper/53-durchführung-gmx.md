\newpage

## GMX

### Versuchsaufbau

Die Untersuchung des GMX Web Mail Clients wurde unter Windows 10 (Version 1809 - Enterprise) im IP Adressbereich der Otto-von-Guericke-Universität Magdeburg durchgeführt. Für die Mehrzahl der Untersuchungen wurde ein neu installierter Chromium Browser verwendet (Version 77.0.3824.0). An den Voreinstellungen des Browsers wurden keine Veränderungen vorgenommen. Als einziges zusätzlich installiertes Plugin kam uMatrix für Chrome[^uMatrixChromium] (Version 1.3.16) zum Einsatz. Dieses Plugin wurde so eingestellt, dass es Verbindungen visualisiert, jedoch nicht blockiert.

Einige Testergebnisse wurden mit zwei anderen Set-ups verglichen.

1. Chromium Browser mit voll aktiviertem uMatrix Plugin.
2. Neu installiertem Firefox Developer Browser (Version 68.0b10 64bit), an dem keinerlei Änderungen vorgenommen und keine Plugins installiert wurden.

### Ergebnisse

Das GMX Portal zeigt offensichtlich auf jeder Seite große Mengen an Drittanbieter Werbung an. Ein Aufruf der Startseite von gmx.net lädt im Schnitt 5,5 MB unterschiedlicher Medien herunter. Eine Untersuchung durch PrivacyScore zeigt 21 bekannte Tracker auf der Startseite, jedoch keine bekannten Sicherheitslücken[^gmxPrivacyScore].

### Drittanbieter
Nach einem Login und während des Lesens und Schreibens von E-Mails sind sehr viele Drittanbieter Ressourcen in die Websites eingebunden. Direkt nach einem Login ist die Anzahl noch geringer. Eine genaue Übersicht über alle Drittanbieter und eine Reproduktion der Ergebnisse ist schwer, da sich diese nach jedem Neu laden der Seite ändern (es werden andere Werbepartner eingebunden). Im Folgenden eine Übersicht über alle geladenen Ressourcen in E-Mail-Verfassen-View, aufgezeichnet am 12. Juni 2019. Die Ressourcen umfassen die Kategorien: CSS, Grafik, Medien, Skripte, XHR-Calls, Frame und Sonstige, wobei die XHR Calls als **REST Calls** besonders ausgewiesen sind, da sie besonders viele Daten transportieren können:

* 1rx.io: 2
* 2mdn.net: 9
* ad-sev.net: 5
* adform.net: 32
* adition.com: 28
* adnxs.com: 62 (davon 1 REST Call)
* adsrvr.org: 6
* adsrvx.com: 20
* amazon-adsystem.com: 19 (davon 9 REST Calls)
* atdmt.com: 2
* bing.com: 4
* bitdefender.de: 2
* casalemedia.com: 16
* cloudflare.com: 1
* conrad.de: 3
* criteo.com: 10 (davon 8 REST Calls)
* criteo.net: 2 (davon 1 REST Call)
* research.de.com: 20
* de17a.com: 4
* doublecklick.net: 54 (davon 4 REST Calls)
* exactag.com: 3 
* facebook.com: 1
* gmxpro.net: 25 (davon 1 REST Call)
* google-analytics.com: 1
* google.com: 4
* google.de: 1
* googlesyndication.com: 32
* gstatic.com: 6
* ioam.de: 8
* lijit.com: 3
* m6r.eu: 4
* mathtag.com: 5
* media1.eu: 3
* mookie1.com 19
* mxcdn.net: 1
* myvisualiq.net: 5
* openx.net: 21 (davon 8 REST Calls)
* pubmatic.com: 6 (davon 1 REST Call)
* rifihub.com: 5
* rubiconproject.com: 1
* serving-sys.com: 36 (davon 4 REST Calls)
* simpli.fi: 3
* sitecount.com: 1
* smartadserver.com: 12 (davon 1 REST Call)
* spotchange.com: 3
* tapad.com: 5
* tidaltv.com: 4
* tifbs.net: 2
* turn.com: 3
* ui-portal.de: 30 (davon 2 REST Calls)
* uimserv.net: 22
* w55c.net: 7
* wayfair.com: 3
* yahoo.com: 4
* yieldlab.net: 5
* youtube.com: 1
* zanox.affiliate.de: 3
* zanox.com: 6

Es wurden bei diesem Test in Summe also 595 Ressourcen von Drittanbietern geladen. Davon waren 40 Ressourcen API Calls gegen Drittanbieter Schnittstellen, die potentiell eine größere Menge an Daten übertragen haben. Inklusive der Inhalte die direkt von gmx.net kamen wurden 7,3 MB heruntergeladen und 1,1 MB hochgeladen und zwar ab dem Zeitpunkt ab dem auf **E-Mail Verfassen** geklickt wurde, über 2 Wechsel aus dem Tab hinaus, bis sich die Seite vollständig geladen hat. Interessant dabei ist, dass nach jedem Wechsel aus dem Browser Tab (entweder in einen anderen Tab, oder aus dem Browser heraus) zahlreiche Drittanbieter Ressourcen nachgeladen werden.

Unter den Drittanbietern sind bekannte Tracker, wie doublecklick.net und die meisten der großen US-Amerikanischen Webunternehmen: Google, Facebook, Microsoft (mit Bing), yahoo!, youtube. Werbetreibende laden ihre Ressourcen teilweise direkt in die Seite (z.B. conrad.de).

![Nach jedem Zurückwechseln in den GMX Tab werden zahlreiche Ressourcen nachgeladen](images/GmxTabWechselNachladen.PNG)

### Versandte Daten

Während der Untersuchung des GMX Webportals konnte zu keinem Zeitpunkt festgestellt werden, dass Echtzeitdaten aufgezeichnet, oder versendet werden. Es werden zwar viele bekannte Tracker eingebunden. Jedoch konnte nicht nachgewiesen werden, dass diese das Nutzerverhalten mitschneiden. Sehr wohl aber werden Werbebanner mit eindeutigen IDs geladen. 

**GMX erhält hierfür die Schulnote 2, da keine Nutzerdaten jenseits von Session Daten direkt an Drittanbieter versendet werden.**

### Mail Versand
Während des Verfassens und Absendens von E-Mails werden keinerlei Daten aus dem Browser gesendet (weder über Websocket-Verbindungen noch über API Calls). Der Inhalt der geschriebenen E-Mail wird lediglich per Post Request an einen GMX Server versendet.

### Sessions

Es werden zu keinem Zeitpunkt Cookies von Drittanbietern gesetzt. Auch der LocalStorage wird nicht durch Drittanbietern befüllt. GMX selber jedoch setzt zahlreiche Cookies, die zur eindeutigen Identifizierung eines Nutzers geeignet sind.

![Auf gmx.net werden keine Drittanbieter Cookies und Lokale Daten gespeichert](images/GmxNoCookies.PNG){height=400}

### Öffnen von E-Mails
Beim Öffnen von E-Mails werde sofort alle Ressourcen (Bilder, Scripte, Fonts, etc.) aus der E-Mail geladen. Ein Tracking ist somit einfach möglich.

### Verschlüsselung

Der Aufruf der Domain gmx.de leitet automatisch auf gmx.net weiter. gmx.net steht ausschließlich als verschlüsselte Version zur Verfügung. Keine Ressourcen werden unverschlüsselt übertragen. Dies umfasst alle API Calls, Medien, JavaScript, CSS Dateien und Fonts von GMX und Drittanbietern.

### Effektivität der Browserhärtung

Das Aktivieren des Browser Plugins uMatrix blockiert das Nachladen aller Drittanbieter Inhalte. GMX weist in diesem Fall mit einem Banner daraufhin, dass JavaScript zur Verwendung der Website eingeschaltet werden soll. Dies ist eine Falschmeldung, da JavaScript lediglich für Drittanbieter blockiert, generell im Browser jedoch zugelassen ist. Die Benutzung der E-Mail Dienste funktioniert auch ohne das Nachladen von Drittanbieterinhalten. Beim Aufruf der Startseite fällt dabei auf, dass GMX lediglich von ```ui-portal.de``` nachladen möchte, wenn das Plugin aktiviert ist. Andere Verbindungsversuche werden nicht unternommen. Zahlreiche Werbebanner zeigen keine Bilder. Die übertragene Datenmenge reduziert sich von ca. 5,5 MB auf ca. 1,2 MB.

### Unterschiede in Firefox
Es konnte in keinem der Szenarien ein signifikanter Unterschied zwischen einem nicht modifizierten Firefox und einem Chromium Browser festgestellt werden. Ein exakter Vergleich ist aufgrund des fehlenden Determinismus der Drittanbietereinbindungen nicht möglich. Die bekanntesten Tracker (z.B. doublecklick.net) wurden in beiden Browsern nicht geblockt.

### Datenschutzrichtlinien

GMX wirbt offensiv mit deutschen Datenschutzstandards. Beim Aufruf der Startseite wird auf die Verwendung von Cookies hingewiesen, jedoch gibt es keine weiteren Einstellungen für die Cookies. 

### Transparenz der Richtlinien

Die GMX Datenschutzerklärung besteht aus ca. 13,900 Wörtern und fasst die Bestimmungen für mehrere Angebote (z.B. Media Center und Freemail) zusammen[^GmxDatenschutzbestimmungen]. In Abschnitt 2.2. erklärt GMX, dass Inhaltsdaten, also E-Mails, Kontakte und hochgeladene Daten verarbeitet werden. Es ist nicht ersichtlich ob dies bedeutet, dass die Daten einfach nur gespeichert werden, oder ob sie zur weiteren Analyse des Nutzerverhalten herangezogen werden. Allerdings wird in Abschnitt 2.4. erklärt, dass generell Analysewerkzeuge zum Einsatz kommen.

GMX zeigt personalisierte Werbung an. Es ist möglich dies abzuschalten. Der Weg zu dieser Abschaltung ist versteckt hinter: ```GMX Mein Account > Kommunikationsprofil > Nutzungsbasierte Werbung ```. In wie weit die Abschaltung funktioniert wurde im Rahmen dieser Arbeit nicht analysiert.

Beim Benutzen des Online Portals speichert GMX den Verlauf der Session. Dies umfasst u.a. das Speichern von Aufrufen einzelner Elemente auf der Seite (vergleiche Datenschutzrichtlinien Abschnitt 3.1.2). Eine Abschaltung oder Teilabschaltung von Cookies ist nicht möglich.

**GMX erhält hierfür die Schulnote 3, da die Bestimmungen zwar ohne Fremdwörter auskommen, aber dennoch wichtige Sektionen nicht hervorheben.**

![GMX verarbeitet laut den Datenschutzbestimmungen den Inhalt von E-Mails, Kontakten und Dateien](images/GmxInhaltsdaten.PNG){height=350}

### Fazit
GMX trackt Nutzer um personalisierte Werbung anzeigen zu können. Dies ist in den Datenschutzbestimmungen eindeutig definiert und kann der DSGVO entsprechend abgeschaltet werden. Die große Anzahl an Werbung durch Drittanbieter auf allen Seiten erweckt sehr schnell einen unseriösen Eindruck und kann Einfalltor für Schadcode sein. Dennoch erfüllt die Website auf Client Seite gängige Sicherheitsanforderungen, da alle Daten verschlüsselt übertragen werden. Auch wird auf der Seite selbst das Verhalten nicht live getrackt, z.B. in Form von Tastenanschlägen. Sehr wohl jedoch gibt es personalisierte Angebote und Drittanbieter erfahren potenziell, wenn Nutzer E-Mails lesen, oder versenden. Über den Inhalt der empfangenen, oder verfassten E-Mails haben sie jedoch keinerlei Kenntnis. 

[^uMatrixChromium]: https://chrome.google.com/webstore/detail/umatrix/ogfcmafjalglgifnmanfmnieipoejdcf?hl=de abgerufen am 28.06.2019
[^GmxRegistrierung]: https://registrierung.gmx.net/#.pc_page.homepage.index.loginbox_1.registrierung abgerufen am 28.06.2019
[^GmxDatenschutzbestimmungen]: https://agb-server.gmx.net/datenschutz#datenschutz_freemail abgerufen am 28.06.2019
[^gmxPrivacyScore]: https://privacyscore.org/site/30819/ abgerufen am 28.06.2019

> Jonas Hielscher  
> `benedikt.hielscher@st.ovgu.de`
