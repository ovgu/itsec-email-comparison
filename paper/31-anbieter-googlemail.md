## Google Mail

### Gmail

Die Googlemail ist mit über einer Milliarde Nutzern die weltweit meist benutzte Plattform zum Versenden und Empfangen von Emails. Die werbefinanzierte Oberfläche ist seit dem 1. April 2004 verfügbar und zeigt ein ständiges Wachstum. Jedoch ist Gmail in Sachen 
Datenschutz schon mehr als einmal in das Visier der Datenschützer geraten.

![](images/Gmail-MailAnbieter.png)
 


#### Kritik

App-Entwickler, die Teil des Gmail-Programms von Google sind, dürfen diese E-Mails unter bestimmten Umständen lesen, um neue Dienste oder App-Funktionen zu erstellen, heißt es. Der Großteil der E-Mails würde automatisch per Computersoftware gescannt, in einigen Fällen sollen aber menschliche Mitarbeiter mitgelesen haben. Return Path, ein Unternehmen, das sich E-Mail-Marketing auf die Fahnen geschrieben hat, hat angeblich die Posteingänge von mehr als 2 Millionen Menschen gescannt und 8000 E-Mails ausspionieren lassen.

### Funktionen

 Der Zugriff auf die E-Mails erfolgt mit einem E-Mail-Programm über TLS-POP3 und TLS-SMTP; seit Oktober 2007 ist der Abruf via IMAP möglich.
 Die Ablage empfangener E-Mails erfolgt nicht, wie bisher allgemein üblich, in verschiedenen Ordnern, sondern in einem zentralen Mailarchiv. Ferner werden Nachrichten in Themen, von Google „Konversationen“ genannt, zusammengefasst. An die Stelle von Ordnern treten bei Gmail sogenannte „Labels“, die frei definiert und per Mailfilter oder manuell den Nachrichten zugeteilt werden können. Durch diese Labels ist es möglich, Mails – im Gegensatz zur gewöhnlichen Ordnerstruktur – mehreren Kategorien zuzuordnen.
 Der wesentliche Unterschied von Gmail zu anderen Freemail-Diensten besteht im Funktionsangebot der browserbasierten Oberfläche, das sich an eigenständigen E-Mail-Programmen (wie zum Beispiel Outlook oder Thunderbird) orientiert. Dieses wurde in großen Teilen mit einer als Ajax bezeichneten Technik in JavaScript sowie DHTML realisiert und umfasst ein Adressbuch, eine Rechtschreibprüfung sowie weitere per Tastenkombination zugängliche Funktionen. Diese sind ähnlich schnell und komfortabel wie ein lokal installiertes Mail-Programm, da die Funktionen größtenteils clientseitig, das heißt auf dem lokalen Rechner, ausgeführt werden.

 Außerdem lässt sich die Weboberfläche von Gmail um meist nützliche Funktionen erweitern, indem man neue Funktionen in den Gmail Labs aktiviert. Zudem existiert eine Vielzahl von autorisierten und nicht-autorisierten Erweiterungen für Gmail. Beispielsweise gibt es Nachrichten-Prüfer zur Anzeige der derzeitigen Anzahl neuer Nachrichten oder Programme wie GmailFS, die Gmail-Konten als virtuelle Laufwerke nutzbar machen. Um die Verwendung von Gmail auch im Offline-Modus zu gewährleisten, benutzte der Dienst seit Anfang 2009 Gears, das im November 2011 eingestellt wurde. Um mehr Nutzer zu erreichen, wird dafür seitdem auf HTML5 gesetzt.

 Eine gerade bearbeitete Mail wird von Gmail automatisch zwischengespeichert, sodass bei Verbindungsabbrüchen oder Zeitüberschreitungen nur Teile des geschriebenen Textes verloren gehen können. Auch wird durch eine Sicherheitsabfrage überprüft, ob ein Seitenwechsel vom Benutzer beabsichtigt ist, sofern auf diese Weise ungespeicherter Text verloren gehen würde.

 Auf Mobilgeräten wird eine spezielle Benutzeroberfläche angezeigt. Diese umfasst zahlreiche Funktionen der Gmail-Desktop-Oberfläche, aber angepasst auf kleinere Bildschirme.


### Sicherheit

 Gmail nutzt die neuesten Sicherheitsstandards. Durch Pishing-Angriffe können Hacker mit viel Aufwand jedoch trotz Zwei-Faktor-Authentifizierung Mails abfangen und ein Konto knacken. 

 Mit seinem neuen Feature „Advanced Protection" schwingt Google sich zum wohl sicheresten Anbieter der Welt auf. Ein USB-Stick für den Computer und ein Bluetooth-Chip für mobile Geräte dienen als Schlüssel. Ohne den Schlüssel ist es für Hacker unmöglich, sich in Ihr Konto einzuloggen. Dieses Feature schafft ein hohes Maß an Sicherheit. Für Privatpersonen sind die Standard-Sicherheitsfeatures von Gmail jedoch vollkommen ausreichend.
 
 ![](images/GmailSicherheit.jpg)
 


### Email Versand und Empfang


- Sie schreiben Ihre Mail und klicken auf Senden dann teiltdas E-Mailprogramm die Mail nun in "Header" und "Body" auf. Im "Header" stehen relevante Informationen wie Absender, Empfänger usw., der "Body" enthält den eigentlichen Text.
- Wenn die Mail Sonderzeichen (ä,ö,ß,&...) enthält, werden diese umgewandelt, codiert.
- Enthält die Mail einen Anhang (Foto, Video, MP3...) wird auch dieser codiert. Der Virenscanner wird aktiv, prüft den Anhang auf Schädlinge.
- Das E-Mailprogramm nimmt Kontakt zum eigenen Mailserver auf und der Mailserver überprüft, ob die Mail zu groß ist. Ist das der Fall, geht sie mit einer Fehlermeldung an den Absender zurück.
- Der Inhalt der Mail wird vom Mailserver gespeichert und derer eigene Mailserver versucht den Mailserver des Empfängers im Netz ausfindig zu machen.
- Dann endlich geht die Mail auf die Reise. Der eigene Mailserver verschickt die Mail an den nächsten Verteilerknoten. Von da geht es über weitere Knoten zum Mailserver des Empfängers.
- Kurz vor dem Ziel nimmt die Mail Kontakt zu dem Empfänger-Mailserver auf. Antwortet dieser, ist alles gut. Antwortet er nicht, folgen noch ein paar Versuche. Klappt's dann immer noch nicht, geht die Mail mit Fehlermeldung zurück an den Absender



#### Verschlüsselte Verbindungen

 Das Versenden von Emails findet bei Gmail über eine gesicherte und verschlüsselte HTTPS Verbindung statt. Das diese wirklich verschlüsselt ist, erkennt man unter anderem auch an dem “s” im https zu Beginn der URL. Bei der Datenübertragung zwischen Ihnen und den Google Servern sind Ihre Mails somit völlig abhörsicher. 


### Datenschutzerklärung 

- Benutzer kann Inhalte aus bestimmten Google-Diensten löschen.
- In manchen Fällen behalten sie Daten für eine begrenzte Zeit zurück, wenn dies zu legitimen Geschäftszwecken oder aus rechtlichen Gründen nötig 
ist. 
- Google nutzt die von uns im Rahmen ihrer Dienste erhobenen Daten für Breitstellung ihrer Diente.

![](images/GmailDatenschutz.png)
 

> Rawan Jouli  
> `rawan.jouli@st.ovgu.de`
