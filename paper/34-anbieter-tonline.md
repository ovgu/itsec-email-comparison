\newpage

## T-Online

### Zahlen und Fakten
T-online Mail, seit einiger Zeit auch Telekom Mail genannt, besitzt im deutschen Markt der E-Mail-Anbieter einen Anteil von 10,8%[^ProviderStatistik] und wird damit in Deutschland von 1 von 10 E-Mail-Nutzern verwendet. Dadurch können Probleme bei Datenschutz und Sicherheit potenziell einige Millionen Menschen treffen, wodurch die vorhandene Kritik, allen voran die fehlende 2-Faktor-Authentifizierung und Anfälligkeit für Phishing-Attacken, besonders dringlich ist.

Heutzutage muss man den Eindruck von der Startseite t-online.de von dem von den Telekom-Diensten unterscheiden, da t-online.de 2015 von der Telekom an die Ströer Content Group verkauft wurde[^Verkauf] und damit nun diese die Webseite verwalten. Ströer betreibt die Startseite heute als Nachrichtenportal, jedoch können Telekom-Kunden dort weiterhin den Zugriff auf ihre E-Mail-Konten und die Telekom-Medien- und Kundencenter vorfinden. Ein Login-Button ist oben rechts auf der Startseite vorzufinden. Der E-Mail-Dienst sowie Medien- und Kundencenter werden weiterhin von der Deutschen Telekom betrieben.

T-online Mail bietet ebenfalls wie GMX sichere DE-Mails an.

#### Gründung
Das Portal wurde im September 1995 gegründet.

#### Kritik
Obwohl sich Ratgeber im Allgemeinen dazu auf den Telekom-Hilfeseiten finden lassen[^2FAArtikel], bietet die Telekom selber bis heute noch keine 2-Faktor-Authentifizierung[^kein2FA] für ihre Dienste an. Darüber beschweren sich die Nutzer in Foren und es herrschen Diskussionen[^UserKritik]. Zwar wurde am 22.01.2019 offiziell im eigenen Forum verlauten lassen, dass diese Funktion wohl demnächst "generell für alle Telekom Login Seiten eingeführt" werden soll[^2FAAngekündigt], bis heute hat sich aber noch nichts getan. Damit liegt T-online Mail in diesem Bereich weit hinter der Konkurrenz, wie GMX, Hotmail, GMail und Yahoo!, zurück, die dies seit einem bzw. mehreren Jahren anbieten.
Dies ist besonders bedauerlich, da T-online Kunden in jüngster Vergangenheit Ziele von Phishing-Attacken wurden[^T-onlinePhishing], wobei 2-Faktor-Authentifizierung eine wichtige zusätzliche Schutzmaßnahme darstellen würde.
Leider fehlt ebenso eine Funktion, sich E-Mails bei einem Login von einem neuen, unbekannten Gerät zuschicken zu lassen, um eventuelle Übernahmeversuche erkennen zu können.[^UserKritik]
Datenschützer kritisieren die aufdringliche und massenhaft eingebundene Werbung und Trackingmaßnahmen, die ebenso von Ströer auf der Startseite wie auch von der Telekom im Postfach eingesetzt werden.
Dazu kommt die automatische Anmeldung zum Telekom Freemail Newsletter bei Registrierung, wodurch Vertragsunterbreitungen direkt im Postfach landen.

### Funktionen
Die Telekom bietet viele verschiedene Dienste an, darunter Kundencenter als Verwaltungshub, Mail, Magenta-Dienste, wie MagentaCloud und MagentaMusik, sowie Mobilfunk-, Festnetz-, Internet- und TV-Tarife.
Für die einfache Verwaltung aller in Anspruch genommenen Dienste und den bequemen Wechsel zwischen ihnen auf einem Gerät, gibt es den Telekom Login. So soll man sich nur einen Benutzernamen und ein Passwort merken müssen, zum Zwecke des Komforts.
Als Nutzer von T-online Mail kann man zwischen 2 Vertragsoptionen wählen. Freemail ist kostenlos und bietet 1 GB Postfachspeicher sowie die Möglichkeit bis zu 100 Mails pro Tag bzw. 1.000 Mails pro Monat versenden zu können. Dazu soll ein Spamschutz der Telekom das Postfach rein halten. Mail M ist die Premiumoption "für höchste Ansprüche" für 2,95 € monatlich. Dadurch bekommt man Zugriff auf 15GB Speicher, den konfigurierbaren "Spamschutz Plus" und die Möglichkeit 5000 Mails pro Tag zu verschicken. Außerdem soll so die Werbung im Postfach ausgeschalten werden[^MailInfo].

### Sicherheit
#### Eigene Angaben
Die Telekom ist Gründungsmitglied der Brancheninitiative "E-Mail made in Germany". Dadurch sind bei T-online Mail Sicherheitsmaßnahmen, auf die sich die großen E-Mail-Anbieter geeinigt haben, kostenlos automatisch aktiviert und nicht abschaltbar. So sollen alle Mails immer SSL-verschlüsselt übermittelt werden und nur in gesicherten Rechenzentren in Deutschland und nach strengen deutschen Datenschutzstandards gespeichert sein[^MadeInGermany]. Dies wird zwischen den Mitgliedern der Initiative, also Telekom, GMX, Web.de, Freenet, 1&1 und Strato, garantiert.[^MadeInGermany]. Leider bleibt offen, ob die Mails in den Rechenzentren auch verschlüsselt gespeichert werden. Es wird darauf hingewiesen, darauf zu achten, dass SSL-Verschlüsselung vom jeweiligen Endgerät und Programm unterstützt wird[^verschl].

Darüber hinaus unterstützt T-online Mail sogar DE-Mail, welches eine weiter entwickelte Version der "E-Mail made in Germany" sein soll. Diese soll neben der sicheren Datenübertragung und der Verarbeitung der Daten in deutschen Rechenzentren zusätzlich die einwandfreie Identität von Sender und Empfänger gewährleistet[^deMail]. DE-Mail Sendungen sollen dadurch gesetzlich rechtssicher sicher. [^deMailTelekom].
Dafür ist eine kostenlose Anmeldung für DE-Mail mithilfe der eigenen t-online.de-Adresse unter https://www.de-mail.t-online.de nötig.

#### Unverschlüsselte Verbindungen
Alle Inhalte im Login-Bereich und im Postfach, sowohl von T-online-Servern als auch von Drittanbietern, werden über verschlüsselte Verbindungen übertragen.

![Chromium Übersicht der Verschlüsselung bei Übertragungen](images/TonlineAllesSicherÜbertragen.PNG)

#### Sicherheitsfeatures
Nachfolgend wurden einige Eigenschaften der Webseite "email.t-online.de" zusätzlich mit entsprechenden Online-Tools untersucht. Dabei ist zu beachten, dass ein Browser ohne eingeloggten Nutzer bei Aufruf von email.t-online.de auf die Startseite t-online.de weitergeleitet wird, um sich über den Login-Button anmelden zu können. Dies kann die Tests der Online-Tools verfälschen, da sie eine Website testen, die theoretisch von einem anderen Anbieter verwaltet wird.

Bei einem Verbindungsversuch über das unverschlüsselte http://email.t-online.de wird automatisch auf die verschlüsselte Version weitergeleitet, vorausgesetzt man ist eingeloggt. Ansonsten landet man auf jedoch auf der verschlüsselten Version der Startseite.[^httpstatus]

Dem Qualys SSL Labs Report zufolge[^sslLabTonline], bietet email.t-online.de mit einem A+ Score eine sehr gute Unterstützung aktueller Sicherheitsfunktionen in der Datenübertragung im Browser.

Laut hstspreload verwendet zumindest t-online.de kein HSTS[^hsts]. Email.t-online.de kann nicht über dieses Tool geprüft werden, da Subdomains nicht unterstützt werden. Es ist unklar, ob die Erkenntnis übertragbar ist, da die Subdomain von einem anderen Anbieter verwaltet wird.

Schutz vor Spam und auch Viren soll der eingebaute Spam-Schutz gewährleisten[^MailInfo]. Dadurch sollen unerwünschte und gefährliche E-Mails erst gar nicht in den Posteingang gelangen. 

Eine Ende-zu-Ende-Verschlüsselung von versendeten Mails über PGP ist möglich, allerdings muss dazu das Browser-Plugin Mailvelope von einem Drittanbieter installiert werden[^t-onlinePGP].

Die Passworterstellung erfordert die Erfüllung von gewissen Sicherheitskriterien und lässt dadurch beliebte schwache Passwörter nicht zu. Das Passwort muss mindestens 8 Zeichen und mindestens 2 der folgenden Bedingungen erfüllen: Kleinbuchstaben, Großbuchstaben, Ziffern, Sonderzeichen.
Damit ist allerdings immer noch zum Beispiel "Passwort" möglich. Außerdem darf ein Passwort maximal 16 Zeichen lang sein, was eine veraltete unnötige Begrenzung darstellt.

### Erstellen eines Nutzerkontos
Für die Nutzung des T-online Mailingdienstes wird ein Telekom Account benötigt. Somit ist man dann im Besitz eines All-in-One-Accounts für das Telekom-Ökosystem, selbst wenn man nur Interesse am Mailingdienst hat. Als Telekom-Vertragskunde liegt solch ein Account oft schon vor.
Für die Erstellung wird die Angabe eines Geburtsdatums benötigt. Leider werden aufgrund der "Passwort vergessen"-Funktion eine Antwort auf eine Sicherheitsfrage sowie eine, im Schritt danach per SMS zu verifizierende, Mobilfunknummer gefordert. Bank- oder Ausweisdaten werden in der freien Version allerdings abgefragt.

> Niklas Baier  
> `niklas.baier@st.ovgu.de`

[^ProviderStatistik]: https://de.statista.com/statistik/daten/studie/151754/umfrage/nutzeranteile-von-e-mail-anbietern-in-deutschland/ abgerufen am 28.06.2019
[^Verkauf]: http://www.manager-magazin.de/unternehmen/it/stroeer-kauft-t-online-a-1047997.html abgerufen am 22.05.2019
[^MailInfo]: https://www.telekom.de/unterwegs/apps-und-dienste/kommunikation/telekom-e-mail abgerufen am 28.06.2019
[^MadeInGermany]: https://kommunikationsdienste.t-online.de/email-made-in-germany/ abgerufen am 30.06.2019
[^UserKritik]: https://telekomhilft.telekom.de/t5/E-Mail-Center/Warum-gibt-es-noch-keine-2-Faktor-Authentifizierung-bei-der/td-p/3680345/page/1 abgerufen am 01.07.2019
[^2FAArtikel]: https://www.telekom.com/de/verantwortung/datenschutz-und-datensicherheit/sicher-digital/details/zwei-faktor-authentifizierung-540852 abgerufen am 01.07.2019
[^kein2FA]: https://www.golem.de/news/stiftung-warentest-zweiter-faktor-bei-immer-mehr-internetdiensten-verfuegbar-1903-140012.html abgerufen am 01.07.2019
[^2FAAngekündigt]: https://telekomhilft.telekom.de/t5/E-Mail-Center/Telekom-Mail-2-Faktor-Authentifizierung/td-p/3612865 abgerufen am 01.07.2019
[^T-onlinePhishing]: https://www.t-online.de/digital/sicherheit/id_85126212/phishing-vorsicht-vor-falschen-mails-in-namen-von-t-online-de-.html abgerufen am 01.07.2019
[^verschl]: http://kommunikationsdienste.t-online.de/email/verschluesselung/ abgerufen am 01.07.2019
[^t-onlinePGP]: https://www.t-online.de/digital/sicherheit/id_73814894/mailvelope-sichere-e-mail-verschluesselung-geht-auch-einfach.html abgerufen am 01.07.2019
[^deMailTelekom]: https://www.telekom.de/zuhause/de-mail abgerufen am 01.07.2019
[^deMail]: https://de-mail.info/ abgerufen am 28.06.2019
[^sslLabTonline]: https://www.ssllabs.com/ssltest/analyze.html?d=email.t-online.de zuletzt abgerufen am 02.07.2019
[^hsts]: https://hstspreload.org/?domain=t-online.de zuletzt abgerufen am 02.07.2019
[^httpstatus]: https://httpstatus.io/ zuletzt abgerufen am 02.07.2019
