# Motivation

Diese Arbeit wurde im Rahmen des Wissenschaftlichen Seminars IT-Sicherheit an der Otto-von-Guericke-Universität, unter der Leitung von Prof. Jana Dittmann und Robert Altschaffel angefertigt.

Bei E-Mail Anbietern laufen alle Informationen von Internetnutzern zusammen. Onlinedienste erfordern eine Anmeldung mit einer E-Mail Adresse und senden später Updates an diese Adressen. Da auch sicherheitskritische Funktionen über E-Mails bereitgestellt werden (z.B. das zurücksetzen von Passwörtern) sollten E-Mail Anbieter besonders hohe Sicherheits- und Datenschutzstandards erfüllen.

Die Sicherheit von E-Mail Anbietern z.B. mit Blick auf die Verschlüsselung, oder dem Schutz vor SPAM Mails[@EMailSpam] wurde schon oft beurteilt. Auch werden Anbieter nach Leistungs- und Sicherheitskriterien direkt miteinader verglichen. Diesen Ansatz verfolgen wir auch mit unserer Arbeit: Wir vergleichen eine Reihe der in Deutschland am häufigsten genutzten E-Mail Anbieter nach Kriterien des Datenschutzes, insbesondere in Bezug auf die Einbindung von Drittanbietern in die Web-Clients der Portale.

Ziel ist es zu überprüfen ob die untersuchten Anbieter sich datenschutzkonform verhalten und zu beurteilen ob auch für nicht IT affine Personen eine Benutzung ohne Datenschutzverletzungen  möglich ist. 

> Jonas Hielscher  
> `benedikt.hielscher@st.ovgu.de`
