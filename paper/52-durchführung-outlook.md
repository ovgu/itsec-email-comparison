\newpage

## Outlook

### Ergebnisse
#### Drittanbieter & versandte Daten
Outlook.com verwendet Inhalte von 14 Drittanbieter-Domains, davon sind allerdings 7 von Microsoft selbst und werden für die weitere Auswertung ignoriert.

- live.com (Microsoft)
- adnxs.com
- advertising.com
- bing.com (Microsoft)
- fonts.googleapis.com
- gstatic.com
- microsoft.com (Microsoft)
- office365.com (Microsoft)
- olsvc.com (Microsoft)
- scorecardresearch.com
- skype.com (Microsoft)
- skypeassets.com (Microsoft)
- taboola.com
- yahoo.com

#### Versandte Daten
- adnxs.com: AppNexus, Tracking für Werbezwecke (29 KB)
- advertising.com: Verizon, Vermittlung von Werbung (417 B)
- fonts.googleapis.com: Google Fonts, Schriftarten (215 KB)
- gstatic.com: Google CDN (215 KB)
- scorecardresearch.com: Tracking & Umfragen (12 KB)
- taboola.com: Vermittlung von Werbung (51 KB)
- yahoo.com: Vermittlung von Werbung (904 B)

#### Sessions
Ja, Taboola speichert eine eindeutige User ID namens "taboola global:user-id" unter adnxs.com

#### Schweregrad
Da geöffnete E-Mails eine eindeutige ID haben, die in der URL steht, und Drittanbieter Zugriff auf die URL in Form des Referers haben, erhält Outlook.com eine 4. Da Skripte von adnxs.com als Drittanbieter direkt im Seitenkontext geladen wird, wird Outlook.com auf eine 5 heruntergestuft.

#### Unterschiede in Firefox
In Firefox gibt es gleich viele Drittanbieter, wobei in beiden Browsern die Anzahl von Aufruf zu Aufruf schwankt.

#### Effektivität der Browserhärtung
Es werden bei aktivierter Browserhärtung immer noch adnxs.com, advertising.com und yahoo.com (also 3 Drittanbieter) verwendet, die jedoch ihrerseits keine weiteren Inhalte nachladen.

#### Vollständigkeit
Dieser Punkt muss mit nein beantwortet werden, da weiteren Werbefirmen wie beispielsweise Yahoo das Recht eingeräumt wird, Daten ihrerseits beliebig weiterzuverwenden.

#### Transparenz
Die Datenschutzbedingungen sind stark verschachtelt und relativ lang - um die kompletten Bedingungen zu lesen, muss man sehr viele Blöcke manuell ausklappen. Hat man das mal geschafft, ist die komplette Seite jedoch relativ leicht verständlich. Damit gibt es die Note 4.

> Moritz Marquardt  
> `moritz.marquardt@st.ovgu.de`
