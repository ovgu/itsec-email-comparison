\newpage

# Vorgehensweise

Um diese E-Mail-Anbieter daraufhin zu analysieren, ob die Nutzung bedenklich für den Datenschutz sein kann, werden wir die hier erläuterte Vorgehensweise auf Grundlage bestimmter Szenarien (was wir tun) und Kriterien (was wir beobachten) einsetzen, um alle analysierten Anbieter sachlich vergleichen zu können.

## Versuchsaufbau

Ein Versuch ist für uns das Durchspielen eines Szenarios in einer leeren, ungehärteten Chrome-Sitzung, zusammen mit der Analyse der Unterschiede unter verschiedenen weiteren Umständen (siehe "Ablauf eines Versuchs").

### Browser
#### Chromium
Wir führen den Versuch in erster Linie mit Chromium in der aktuellsten Version durch, da dieser Browser die Ansicht von Nachrichten in Websocket-Verbindungen ermöglicht. Zusätzlich benötigen wir die Erweiterung "uMatrix"[^uMatrix-Chromium], um die Zahl und Domains der Drittanbieter einfach sehen zu können.

Oben rechts gibt es ein Benutzermenü mit dem Eintrag "Nutzer verwalten", über den die Profile verwaltet werden können. Hier können wir vor jeder Versuchsreihe ein neues, unbenutztes Profil anlegen, damit die Versuche wiederholbar sind.

#### Firefox
Wir verwenden zusätzlich Firefox, um Unterschiede zwischen Browsern zu testen. Zudem möchten wir überprüfen, wie effektiv die Firefox-Funktion zur Blockierung der Aktivitätenverfolgung das Tracking durch die E-Mail-Provider verhindert. Zusätzlich benötigen wir auch hier "uMatrix".[^uMatrix-Firefox]

Der Befehl `firefox --ProfileManager --new-instance` startet den Profilmanager (kann ausgeführt werden mit `Windowstaste+R`, oder `Alt+F2` in Linux), in dem wie in Chromium unbenutzte Profile angelegt werden können.

##### Gehärteter Browser
Um eine Empfehlung für möglichst viele Endnutzer aussprechen zu können, verwenden wir Firefox mit der Einstellung "Streng" unter "Datenschutz & Sicherheit" als einzige Härtung unseres Browsers. Wir wollen so testen, ob es eine einfache Möglichkeit gibt, Tracking bei einem Anbieter zu entgehen.

### Zu vergleichende Szenarien

Betrachtet werden die *Anmeldung* bei einem Anbieter (ab dem Besuch der Anmeldeseite bis zum Absenden der Login-Daten), der *Versand* einer E-Mail (ab dem Klick auf den Senden-Button) sowie der *Empfang & Abruf* einer beliebigen E-Mail (Posteingang & E-Mail-Ansicht).

### Ablauf eines Versuchs

uMatrix ist stets auf "Alles erlauben" einzustellen. Zu jedem Szenario gibt es für jedem Anbieter drei Versuche: jeweils einen in Google Chrome, in Firefox, und im gehärteten Browser. Die letzten zwei werden für die Analyse der "Unterschiede in Firefox" und der "Effektivität der Browserhärtung" benötigt.

Für jeden Versuch müssen wir zuerst mit F12 die Developer-Tools öffnen, und im Tab "Netzwerk" (oder "Netzwerkanalyse") das automatische Leeren der Logs deaktivieren. Nun wird das Szenario durchgespielt und daraufhin die entstandenen Daten nach den folgenden Kriterien ausgewertet.

Jeder Versuch wird zur Nachvollziehbarkeit mit Screenshots des uMatrix-Fensters dokumentiert (siehe dazu "Anhang 1"), zusammen mit der jeweilige exakten Vorgehensweise und den Ergebnissen.

### Zu vergleichende Kriterien

#### Drittanbieter
*Wie viele und welche Drittanbieter können an private Daten des Nutzers gelangen?*

Die von uMatrix angezeigte Liste wird hierfür verwendet, zusätzlich werden falls vorhanden Drittanbieter in die Liste aufgenommen, die nur in der Datenschutzerklärung vorkommen.

***Ergebnis:*** Anzahl der Drittanbieter; Liste mit Domains der Unternehmen

#### Versandte Daten
*Wie viele und welche Daten werden tatsächlich an einen Drittanbieter versendet?*

Die Netzwerkverkehrsansicht in den Developer-Tools wird nach der Drittanbieter-Domain gefiltert, um die Datenmenge auswerten zu können. Der Zweck der Daten wird falls möglich den Angaben auf der offiziellen Drittanbieter-Webseite entnommen.

***Ergebnis (je Drittanbieter):*** Größe in KiB; Zweck der Daten

#### Sessions
*Setzen Drittanbieter z. B. Cookies ein, um ihrerseits Nutzer eindeutig zu identifizieren?*

Wir können dies im "Anwendung"-Tab in Chromium und im "Speicher"-Tab in Firefox sehen. Gibt es hier einen scheinbar zufällig generierten Token, identifiziert der Drittanbieter den Nutzer auf eine eindeutige Art und Weise.

***Ergebnis (je Drittanbieter):*** ja/nein (nein bevorzugt)

#### Schweregrad
*Wie kritisch sind die übertragenen Daten im schlechtesten Fall?*

Dies kann aus versandten Daten abgeleitet werden; aufgrund des Umfangs gehen wir nur auf auffällige Verbindungen ein. Eine Herabstufung erfolgt bei Drittanbieter-JavaScript-Inhalten, die im Hauptkontext ausgeführt werden, da diese vollen Zugriff auf alle Inhalte haben - bei aktiviertem "Group by Frame" in Chromium erscheinen diese Skripte in der obersten Ebene. Gibt es nur Anbieter, die direkt im Quellcode der Seite zu finden sind, wird eine Note abgezogen, ansonsten erfolgt eine Herabstufung auf Note 5.

***Ergebnis (Schulnote 1-5):***
**[1]** keine Drittanbieter oder nur IP-Adressen & Browserinformationen (bevorzugt);
**[2]** persönliche Session;
**[3]** persönlich identifizierbare Informationen (beispielsweise Name oder E-Mail-Adresse);
**[4]** Absender oder Betreffzeilen von E-Mails;
**[5]** Inhalte von E-Mails

#### Unterschiede in Firefox
Gibt es Unterschiede bei der Verwendung in Firefox?

Dies weisen wir mit der von uBlock angezeigten Anzahl der Drittanbieter nach.

***Ergebnis:*** Differenz "Drittanbieter Firefox" - "Drittanbieter Chrome" (nahe an 0 bevorzugt)

#### Effektivität der Browserhärtung
Werden tatsächlich keine Daten mehr an Drittanbieter versendet, wenn man einen gehärteten Browser surft? Wenn nein, welche Drittanbieter sind davon betroffen?

Dies testen wir mithilfe der von uBlock angezeigten Liste geladener Drittanbieter..

***Ergebnis:*** Anzahl & Liste übriger Drittanbieter (keine bevorzugt)

#### Vollständigkeit
Werden nur in der Datenschutzerklärung explizit erwähnte Anbieter kontaktiert?

Wird einem Anbieter das Recht eingeräumt, selbst zu entscheiden, welche weiteren Anbieter Zugang zu Daten erhalten können, wird dieser Punkt mit "nein" beantwortet.

***Ergebnis:*** ja/nein (ja bevorzugt)

#### Transparenz
Wie verständlich sind die Datenschutzbestimmungen formuliert?

Dieses an sich subjektive Kriterium soll durch eine Einteilung in genauer spezifizierte Schulnoten möglichst objektiv gehalten werden:

***Ergebnis (Schulnote 1-5):***
**[1]** vereinfachte Version verfügbar;
**[2]** kurz und einfache Sprache;
**[3]** mit genug Zeit nachvollziehbar;
**[4]** unnötig lang oder kompliziert;
**[5]** für Laien unverständlich  
\kern 0.16667em

[^uMatrix-Chromium]: https://chrome.google.com/webstore/detail/umatrix/ogfcmafjalglgifnmanfmnieipoejdcf?hl=de; aufgerufen am 04.07.2019
[^uMatrix-Firefox]: https://addons.mozilla.org/de/firefox/addon/umatrix/; aufgerufen am 04.07.2019

> Moritz Marquardt  
> `moritz.marquardt@st.ovgu.de`
