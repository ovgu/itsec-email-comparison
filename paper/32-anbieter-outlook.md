\newpage

## Outlook

### Nutzerzahlen
Outlook.com hat über 400 Millionen Nutzer[^outlook-nutzer], und ist in Deutschland mit 9,6% der fünftbeliebteste E-Mail-Provider.[^ProviderStatistik]

### Gründung
Der Dienst existiert seit 1995 unter dem Namen "Hotmail", und wurde kurz darauf von Microsoft übernommen.[^outlook-geschichte]

### Kritik
Wie Google auch verwertet Microsoft die Inhalte der E-Mails für weitere Zwecke wie beispielsweise Cortana, im Gegensatz zu Google aber nicht für die Personalisierung von Werbung. Personalisierte Werbung wird bei Outlook dennoch angezeigt, die Daten hieraus stammen jedoch nicht aus den E-Mail-Daten, sondern aus anderen Quellen.[^outlook-datenschutz]

### Funktionen
Das kostenlose Konto umfasst 15 GB Speicherplatz[^outlook-speicher], über die Business-Verträge ist auch der Support für eigene Domains gegeben.[^outlook-domains]
Microsoft wirbt damit, dass E-Mails, Kalender und Dateien miteinander verbunden sind, und so ein einfacherer Workflow möglich ist. Zudem werden E-Mails automatisch nach Kategorien und Relevanzen sortiert.[^outlook-features]


### Sicherheit
#### Eigene Angaben
Es gibt keine besonderen Sicherheitsfunktionen, für die der Anbieter wirbt, aber alle normalen Sicherheitsvorkehrungen werden getroffen. Es existieren zusätzlich weitere Sicherheitsfunktionen wie beispielsweise eine Zwei-Faktor-Authorisierung.

#### Unverschlüsselte Verbindungen
HSTS wird verwendet, um MITM-Angriffen vorzubeugen, outlook.com ist jedoch nicht in der HSTS-Preload-Liste.[^outlook-hsts] Die Seite ist positiverweise nicht per HTTP erreichbar, hier gibt es nur eine Weiterleitung zu HTTPS; der Server erreicht hierbei den Grade A bei den Qualys SSL Labs[^outlook-qualys]. E-Mails werden nicht verschlüsselt gespeichert.

### Erstellen eines Nutzerkontos
Um ein Nutzerkonto zu erstellen, werden die gewünschte E-Mail-Adresse, ein Passwort, Vor- und Nachname, Land und das Geburtsdatum benötigt. Zusätzlich müssen die Datenschutzbestimmungen akzeptiert werden, wobei viele Tracking-Cookies deaktiviert werden können.

Insgesamt ist ein Konto sehr einfach erstellbar und benötigt keinerlei direkt persönlich identifizierbare Informationen (Telefonnummer, Adresse, ...) wie bei vielen anderen Anbietern.

[^outlook-nutzer]: http://winfuture.de/news,75884.html abgerufen am 05.07.2019
[^outlook-geschichte]: http://blogs.office.com/b/microsoft-outlook/archive/2012/07/31/introducing-outlook-com-modern-email-for-the-next-billion-mailboxes.aspx abgerufen am 27.10.2012
[^outlook-speicher]: https://support.office.com/de-de/article/Speicherbegrenzungen-in-Outlook-com-7ac99134-69e5-4619-ac0b-2d313bba5e9e abgerufen am 05.07.2019
[^outlook-domains]: https://support.office.com/en-us/article/Change-your-email-domain-in-Outlook-com-Premium-cc47f494-8679-4365-97c1-e709aebf727e abgerufen am 05.07.2019
[^outlook-datenschutz]: https://privacy.microsoft.com/de-DE/privacystatement abgerufen am 05.07.2019
[^outlook-hsts]: https://hstspreload.org/?domain=outlook.com abgerufen am 05.07.2019
[^outlook-qualys]: https://www.ssllabs.com/ssltest/analyze.html?d=outlook.com&latest abgerufen am 05.07.2019
[^outlook-features]: https://outlook.live.com/owa/ abgerufen am 05.07.2019

> Moritz Marquardt  
> `moritz.marquardt@st.ovgu.de`
