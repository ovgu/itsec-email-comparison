\newpage

# Ausblick

In dieser Arbeit wurden lediglich die Web Clients von vier viel genutzen E-Mail Anbietern untersucht. Alle Anbieter bieten ebenfalls eigene Apps für mobile Betriebssystem an. Diese sind wesentlich schwieriger datenschutzfreundlich zu konfigurieren und ebenfalls schwerer zu untersuchen. Eine Untersuchung dieser Apps ist Relevant für die Gesamtbeurteilung der Anbieter und für das Erstellen finaler Empfehlungen. 

E-Mail Provider werden von nahezu allen Internetnutzern benutzt. Wiederkehrende und u.U. auch automatisierte Untersuchungen wären daher indiziert.

> Jonas Hielscher  
> `benedikt.hielscher@st.ovgu.de`
