
## Anzahl der Drittanbieter
* 7 einzigartige
* more Information: siehe umatrix Screenshot.
## Drittanbieter-Domains & wie viele KB jeweils übertragen werden (Up & Down)
* adition.com 28.5 KB / 4.2
* gmxpro.net
* ioam.de
* tifbs.net
* ui-portal.de
* uicdn.com
* uimserv.net
## Unverschlüsselter Inhalt (ja/nein)
* nein
## Anzahl der Drittanbieter-Cookies (inkl. localStorage)
* keinerlei Drittanbieter Cookies
## Drittanbieter-Cookies (inkl. localStorage)
* keinerlei Drittanbieter Cookies
## aktive Verbindungen (Daten die von Drittanbietern empfangen & verwendet werden)
* keine Websocktverbindung
* keine offenen REST Calls
## kritische Inhalte an Drittanbieter
* ui-portal GET API Calls ohne Id
    * https://img.ui-portal.de/pos-cdn/gmx/atlas/latest/logo/gmx-5c556c41.svg
    * https://img.ui-portal.de/pos-cdn/gmx/atlas/latest/poseidon-7d2e77eb.svg
* gmxpro
    * Potentiell ID mit GET Call an: https://cdn.gmxpro.net/cdn/mail/client/wicket/resource/static-res/---/mc/img/sprite-vEr-5B597AB094B50E00A734027EA8717D87.svg

