## Vorbemerkung
* Send E-Mail ist auf Single Application Page

## Anzahl der Drittanbieter
## Drittanbieter-Domains & wie viele KB jeweils übertragen werden (Up & Down)

## Unverschlüsselter Inhalt (ja/nein)
* nein
## Anzahl der Drittanbieter-Cookies (inkl. localStorage)
* keinerlei Drittanbieter Cookies
## Drittanbieter-Cookies (inkl. localStorage)
* keinerlei Drittanbieter Cookies
## aktive Verbindungen (Daten die von Drittanbietern empfangen & verwendet werden)
* keine Websocktverbindung
* permament nachladender Content nach jedem wechseln in den Tab
## kritische Inhalte an Drittanbieter
* Weder beim Schreiben der E-Mail noch nach dem Absenden gehen API Calls mit E-Mail Inhalt an Drittanbieter.