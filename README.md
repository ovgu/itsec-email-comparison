# Wissenschaftliches Seminar IT-Sicherheit
## E-Mail-Mithörer
## ➜ [Paper](https://get.mo-mar.de/Seminarpaper-E-Mail-Mithörer.pdf) · [Wiki](https://codeberg.org/ovgu/itsec-email-comparison/wiki)
> Niklas Baier, Jonas Hielscher, Rawan Jouli & Moritz Marquardt

In diesem Repository sind alle Abgabedokumente sowie Ressourcen (Bilder, Screenshots, Skripte, usw.) zu finden.

Für Entwurfsdokumente und die Projektplanung, [siehe Projektwiki](https://codeberg.org/ovgu/itsec-email-comparison/wiki).

## Markdown

Die grundlegende (sehr einfache) Syntax für Markdown kann auf https://devhints.io/markdown nachgesehen werden, die vollständige Dokumentation ist auf https://daringfireball.net/projects/markdown/basics verfügbar.

Zusätzlich können wir in `bibliography.yaml` Literaturwerke definieren, die wir dann per `[@id]` referenzieren können - mehr Details dazu gibt es auf https://pandoc.org/MANUAL.html#citations.

Per `[^name]` können wir eine Fußnote referenzieren, die wir dann in einer eigenen Zeile mit `[^name]: Weitere Hinweise` festlegen können. Für alle Infos dazu, siehe https://pandoc.org/MANUAL.html#footnotes.

## Paper exportieren

Die aktuellste Version des Papers (der `master`-Branch) ist immer auf https://get.mo-mar.de/Seminarpaper-E-Mail-Mithörer.pdf verfügbar.

### Vorraussetzungen
- Pandoc
- TeX Live (mit LaTeX Extra & Extra Fonts)
- Eisvogel-Template

### Export
```bash
git clone git@codeberg.org:ovgu/itsec-email-comparison.git  # clone the git repo
cd itsec-email-comparison/paper  # change to "paper" directory
make  # build the paper (creates or updates paper.pdf)
```
